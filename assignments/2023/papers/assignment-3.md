# Assignment

- Course Title: **Trends in Artificial Intelligence and Machine Learning**
- Course Code: **TAI911S**
- Assessment: Third Assignment
- Released on: 08/05/2023
- Due Date: 08/06/2023

# Problem

## Problem Description

This assignment aims to develop a **recommender system**  that can guide students (in a tertiary institution) in selecting their courses instead of simply joining a __rigidly__ defined curriculum. The recommendation is made based on a student's profile (courses he/she completed in the past and his/her career plan) and his/her current level. For example, a degree in **Software Engineering** requires successfully completing courses at four distinct levels. These include core (e.g., Software Methodology, Data Structures and Algorithms) and general-purpose (e.g., Communication Skills) courses. Note that there could be pre-requisite requirements between courses at different levels in the curriculum. 

Your task is to:

1. Complete the design of a flexible curriculum that allows students to fulfil their career plans;
1. Create an appropriate dataset to implement the recommender system;
1. Define and implement a technique based on **Transformers** for the recommender system.

Note that the dataset can be adapted from an existing dataset or generated synthetically. Note also that all your models and implementation must be completed in the **Julia** programming language.

## Assessment Criteria

We will follow the criteria below to assess the problem:

- Design of the programme. (5%)
- Dataset creation and preprocessing. (15%)
- Implementation of the recommender system. (45%)
- Evaluation Metrics. (20%)
- Overall solution in **Julia**. (15%)

# Submission Instructions

- This assignment is to be completed in groups of at most three students.
- For each submission, a repository should be created on [Gitlab](https://about.gitlab.com) with the details of all members.
- The submission date is Thursday, June 08 2023, at midnight. Please note that _commits_ after that deadline will not be accepted. Therefore, a submission will be assessed based on the repository's clone at the deadline.
- Groups which fail to submit will be awarded the mark 0.
- Groups that fail to submit their assignment on time will have their marks deducted for every two days of delay.
- In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions involved will be awarded the mark 0, and each student will receive a warning.
