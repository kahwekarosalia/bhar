# bhar

This repository contains all materials related TAM911S. The materials in this repository include a course outline, slides, tutorials and
practicals and additional resources. We will be using **Julia 1.8.5** (or newer versions) in this course. The practicals in this course will be run using [Julia](https://julialang.org) and the
[Pluto](https://https://plutojl.org) notebook.

To install the **Pluto** notebook, follow the steps below

1. Run Julia in a terminal: `julia`
2. Install Pluto: `import Pkg; Pkg.add("Pluto")`

Alternatively, you can switch to the package mode by pressing the right square bracket key on your keyboard. Then, type `add("Pluto")`. Once the
installation completes, press the backspace key to switch back to normal mode.

To start a Pluto notebook, takes the steps below:

1. `using Pluto`
2. `Pluto.run()`

Note that the `Pluto.run()` command accepts optional parameters. Please check the documentation on the website for more information.
